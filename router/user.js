//引入express包
const express = require("express")
//创建路由对象
const router = express.Router()
//引入处理函数模块
const hander = require("../router_hander/user")


//导入验证规则模块
const expressJoi = require("@escook/express-joi")
//导入写好的规则对象
const { reg_login_schema } = require("../schema/user")


//为该路由对象挂载注册路由
router.post("/register", expressJoi(reg_login_schema), hander.register)
//为该路由对象挂载登录路由
router.post("/login", expressJoi(reg_login_schema), hander.login)


//导出路由对象
module.exports = router