//本文件为文章路由模块

//引入express包
const express = require("express")
//创建路由对象
const router = express.Router()
//导入文章操作相应函数模块
const artcate = require("../router_hander/artcate")
//导入规则对象mysql workbench如何导出数据库
const {add_artcate_schema, del_artcate_schema, get_cate_schema, update_cate_schema} = require("../schema/artcate")
//导入验证规则的包
const expressJoi = require("@escook/express-joi")


//获取文章分类
router.get("/cates", artcate.getArticleCates)
//新增文章分类
router.post("/addcates", expressJoi(add_artcate_schema), artcate.addArticleCates)
//删除文章分类
router.post("/delcates/:id", expressJoi(del_artcate_schema), artcate.delArticleCates)
//根据Id获取文章分类数据
router.get("/cates/:id", expressJoi(get_cate_schema), artcate.getArticleById)
//根据Id更新文章分类数据
router.post("/updatecate", expressJoi(update_cate_schema), artcate.updateCateById)



//导出路由对象
module.exports = router