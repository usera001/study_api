//本文件存放与用户信息查询、修改相关的接口
//导入express包
const express = require("express")
//创建router路由对象
const router = express.Router()


//导入处理函数
const userinfo_hander = require("../router_hander/userinfo")


//导入规则模块
const {update_userinfo_schema, update_password_schema, update_avatar_schema} = require("../schema/user")
//导入验证规则的包
const expressJoi = require("@escook/express-joi")


//获取用户信息
router.get("/userinfo", userinfo_hander.getUserinfo)
//更新用户基本信息
router.post("/userinfo", expressJoi(update_userinfo_schema), userinfo_hander.updateUserInfo)
//修改密码
router.post("/updatepwd", expressJoi(update_password_schema), userinfo_hander.updatePassword)
//更新头像
router.post("/update/avatar", expressJoi(update_avatar_schema), userinfo_hander.updateAvatar)


//导入router对象
module.exports = router