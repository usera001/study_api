//入口文件

//引入express包
const express = require("express")
//创建express服务器实例
const app = express()


//引入并配置cors中间件，解决跨域问题
const cors = require("cors")
app.use(cors())
//配置解析数据的中间件
app.use(express.urlencoded({extends: false}))
//优化响应代码
app.use(function(req, res, next) {
  res.cc = function(message, status = 1) {
    res.send({
      //状态
      status,
      //状态描述：判断参数status是否是Error的实例对象，是则传该实例对象的message属性，否则传字符串
      message: status instanceof Error ? status.message : message
    })
  }
  next()
})
//验证token
const config = require("./config") //导入全局配置文件
const expressJWT = require("express-jwt")//导入验证token包
app.use(expressJWT({ secret: config.jwtSecretKey }).unless({ path: [/^\/api\//] }))//JWT验证


// 托管静态资源文件
app.use('/uploads', express.static('./uploads'))

//注册登录路由
const userRouter = require("./router/user")
app.use("/api", userRouter)
//用户信息查询路由
const userinfo = require("./router/userinfo")
app.use("/my", userinfo)
//文章分类路由
const artcate = require("./router/artcate")
app.use("/my/article", artcate)
 //文章路由
 const articleRouter = require('./router/article')
 app.use('/my/article', articleRouter)




//捕获错误
app.use((err, req, res, next) => {
  //判断身份是否过期
  if(err.name === "UnauthorizedError") return res.cc("身份验证过期，请重新登录！")
  //其他错误
  res.cc(err)

})

//监听特定端口
app.listen(3007, () => {

  console.log('api server running athttp://127.0.0.1:3007')

})