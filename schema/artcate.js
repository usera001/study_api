//文章分类表单验证规则
const joi = require("joi")

//定义规则
const name = joi.string().required()
const alias = joi.string().alphanum().required()
const id = joi.number().integer().min(1).required()

//验证规则——新增文章分类
exports.add_artcate_schema = {
  body: {
    name,
    alias
  }
}

//验证规则——删除文章分类
exports.del_artcate_schema = {
  params: {
    id
  }
}

//验证规则——根据Id获取文章分类数据
exports.get_cate_schema = {
  params: {
    id
  }
}

//验证规则——根据Id更新文章分类
exports.update_cate_schema = {
  body: {
    Id: id,
    name,
    alias
  }
}