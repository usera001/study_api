//本文件定义了验证用户信息的规则

//导入规则模块
const joi = require("joi")

//定义规则
const username = joi.string().alphanum().min(4).max(10).required()
const password = joi.string().pattern(/^[\S]{6,12}$/).required()
const id = joi.number().integer().min(1).required()
const nickname = joi.string().max(20).required()
const email = joi.string().email().required()
const avatar = joi.string().dataUri().required()


//导出登录注册规则对象
exports.reg_login_schema = {
  body: {
    username,
    password,
  },
}

//验证规则——更新用户信息
exports.update_userinfo_schema = {
  body: {
    id,
    nickname,
    email
  }
}

//验证规则——修改密码
exports.update_password_schema = {
  body: {
    oldPwd: password,
    newPwd:joi.not(joi.ref("oldPwd")).concat(password)
  }
}

//验证规则——更新头像
exports.update_avatar_schema = {
  body: {
    avatar
  }
}