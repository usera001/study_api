//引入数据库模块
const db = require("../db/index")
//引入加密工具
const bcryptjs = require("bcryptjs")
//导入生成token的包
const jwt = require("jsonwebtoken")
//导入全局配置文件
const config = require("../config")

//用户注册处理函数
exports.register = (req, res) => {
  const userinfo = req.body
  //优化后此处不需要
  // if(!userinfo.username || !userinfo.password)
  // {
  //   return res.cc("账号或密码不能为空")
  // }
  
  //定义查询命令
  const sqlStr = "SELECT * FROM ev_users WHERE username = ?"

  //查询数据
  db.query(sqlStr, userinfo.username, (err, result) => {
    if(err)
    {
      //执行SQL失败
      return res.cc(err)
    }

    //判断用户名是否存在
    if(result.length > 0)
    {
      return res.cc("用户名已存在！")
    }

    //加密密码
    userinfo.password = bcryptjs.hashSync(userinfo.password, 10)

    //提交数据
    db.query("INSERT INTO ev_users SET ?", {username: userinfo.username, password: userinfo.password}, (err, results) => {
      //判断sql代码是否执行失败
      if(err) return res.cc(err)
      //判断是否正确插入
      if(results.affectedRows != 1) return res.cc("注册失败，请重试！")
      //注册成功执行
      res.cc("注册成功！", 0)
    })

  })
}

//用户登陆处理函数
exports.login = (req, res) => {
  //获取用户提交的数据
  const userinfo = req.body
  //查询用户账号密码
  db.query("SELECT * FROM ev_users WHERE username = ?", userinfo.username, (err, results) => {

    //判断SQL语句是否成功执行
    if(err) return res.cc(err)
    //判断查询数据条数
    if(results.length !== 1) return res.cc("该用户未注册！")
    //判断密码是否正确
    const compareResult = bcryptjs.compareSync(userinfo.password, results[0].password)
    if(!compareResult) return res.cc("密码错误！")

    //登录成功执行
    //处理用户信息：剔除密码与头像
    const user = {...results[0], password: "", user_pic: ""}
    //生产token
    const token = jwt.sign(user, config.jwtSecretKey, { expiresIn: config.expiresIn })
    //响应客户端信息
    res.send({
      status: 0,
      message: "登录成功！",
      token: "Bearer " + token,
    })
    
  })

}