//文章操作相应函数

//导入数据库模块
const db = require("../db/index")



//获取文章分类
exports.getArticleCates = (req, res) => {
  //获取数据
  db.query("SELECT * FROM ev_article_cate WHERE is_delete = 0 ORDER BY id ASC", (err, results) => {
    //sql执行失败
    if(err) return res.cc(err)
    //成功执行
    res.send({
      status: 0,
      message: "请求成功!",
      data: results
    })
  })
}

//新增文章分类
exports.addArticleCates = (req, res) => {
  //判断文章分类名称与别名是否存在
  db.query("SELECT * FROM ev_article_cate WHERE name = ? OR alias = ?", [req.body.name, req.body.alias], (err, results) => {
    //sql执行失败
    if(err) return res.cc(err)
    //成功执行：判断数据是否存在
    if(results.length === 2) return res.cc("文章分类名称与别名均已存在！")
    if(results.length === 1 && results[0].name === req.body.name && results[0].alias === req.body.alias) return res.cc("文章分类名称与别名均已存在！")
    if(results.length === 1 && results[0].name === req.body.name) return res.cc("文章分类名称已存在！")
    if(results.length === 1 && results[0].alias === req.body.alias) return res.cc("文章分类别名已存在！")
    //新增文章
    db.query("INSERT INTO ev_article_cate (name, alias) VALUES (?, ?)", [req.body.name, req.body.alias], (err, results) => {
      //sql语句执行失败
      if(err) return res.cc(err)
      //sql语句执行成功，数据插入失败
      if(results.affectedRows !== 1) return res.cc("新增文章分类失败！")
      //成功执行
      res.cc("新增文章分类成功！", 0)
    })
  })
}

//删除文章分类
exports.delArticleCates = (req, res) => {
  db.query("UPDATE ev_article_cate SET is_delete = 1 WHERE id = ?", req.params.id, (err, results) => {
    //sql语句执行失败
    if(err) return res.cc(res)
    //sql语句执行成功，但影响行数不为一
    if(results.affectedRows !== 1) return res.cc("删除文章分类失败！")
    //成功执行
    res.cc("删除文章分类成功！")
  })
}

//获取文章分类数据
exports.getArticleById = (req, res) => {
  db.query("SELECT * FROM ev_article_cate WHERE id = ?", req.params.id, (err, results) => {
    //sql语句执行失败
    if(err) return res.cc(err)
    //sql语句执行成功，但是没有数据
    if(results.length !== 1) return res.cc("获取分类失败！")
    //成功执行
    res.send({
      status: 0,
      message: "获取分类数据成功！",
      data: results[0]
    })
  })
}

//根据Id更新文章分类
exports.updateCateById = (req, res) => {
  //判断文章分类名称与别名是否存在
  db.query("SELECT * FROM ev_article_cate WHERE Id <> ? AND (name = ? or alias = ?)", [req.body.Id, req.body.name, req.body.alias], (err, results) => {
    //sql语句执行失败
    if(err) return res.cc(err)
    //成功执行：判断数据是否存在
    if(results.length === 2) return res.cc("文章分类名称与别名均已存在！")
    if(results.length === 1 && results[0].name === req.body.name && results[0].alias === req.body.alias) return res.cc("文章分类名称与别名均已存在！")
    if(results.length === 1 && results[0].name === req.body.name) return res.cc("文章分类名称已存在！")
    if(results.length === 1 && results[0].alias === req.body.alias) return res.cc("文章分类别名已存在！")
    //更新文章分类
    db.query("UPDATE ev_article_cate SET name = ?, alias = ? WHERE Id = ?", [req.body.name, req.body.alias, req.body.Id], (err, results) => {
      //sql语句执行失败
      if(err) return res.cc(err)
      //sql语句执行成功，但影响行数不为一
      if(results.affectedRows !== 1) return res.cc("修改失败！")
      //成功执行
      res.cc("修改成功！", 0)
    })
  })
}