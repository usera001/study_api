//本文件存放路由中的处理函数

//导入数据库模块
const db = require("../db/index")
//导入bcryptjs包
const bcryptjs = require("bcryptjs")

//获取用户信息
exports.getUserinfo = (req, res) => {
  //请求数据
  db.query("SELECT  id, username, nickname, email, user_pic FROM ev_users WHERE id = ?", req.user.id, (err, results) => {
    //判断sql语句是否正常执行
    if(err) return res.cc(err)
    //判断查询到的数据条数是否为1
    if(results.length !== 1) return res.cc("获取用户信息失败!")
    //成功执行 
    res.send({
      status: 0,
      message: "用户信息获取成功！",
      data: results[0]
    })
  })
}

//更新用户基本信息
exports.updateUserInfo = (req, res) => {
  //更新数据
  db.query("UPDATE ev_users SET ? WHERE id = ?", [req.body, req.body.id], (err, results) => {
    //检测sql语句是否正确执行
    if(err) return res.cc(err)
    //执行SQL语句成功，但影响行数不为1
    if(results.affectedRows !== 1) return res.cc("修改信息失败！")
    //成功执行
    res.cc("修改成功！", 0)
  })
}

//修改密码
exports.updatePassword = (req, res) => {
  //获取用户信息（即判断用户是否存在）
  db.query("SELECT *FROM ev_users WHERE id = ?", req.user.id, (err, results) => {
    //判断sql语句是否正确执行
    if(err) return res.cc(err)
    //sql语句执行成功，判断用户是否存在
    if(results.length !== 1) return res.cc("用户不存在！")
    //判断密码是否正确
    const compareResult = bcryptjs.compareSync(req.body.oldPwd, results[0].password)
    if(!compareResult) return res.cc("旧密码错误！")
    //修改密码
    req.body.newPwd = bcryptjs.hashSync(req.body.newPwd, 10)
    db.query("UPDATE ev_users SET password = ? WHERE id = ?", [req.body.newPwd, req.user.id], (err, results) => {
      //sql语句执行失败
      if(err) return res.cc(err)
      //sql语句执行成功，判断影响数据条数
      if(results.affectedRows !== 1) return res.cc("修改失败！")
      //修改成功
      res.cc("修改成功！", 0)
    })
  })
}

//更新头像
exports.updateAvatar = (req, res) => {
  //修改头像
  db.query("UPDATE ev_users SET user_pic = ? WHERE id = ?", [req.body.avatar, req.user.id], (err, results) => {
    //sql语句执行失败
    if(err) return res.cc(err)
    //sql语句执行成功，修改头像失败
    if(results.affectedRows !== 1) return res.cc("修改失败！")
    //成功执行
    res.cc("修改成功！", 0)
  })
}